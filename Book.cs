﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using System.IO;




namespace BookStore
{
    /// <summary>
    /// Book product class. Inherited from Product class.
    /// </summary>
    public class Book:Product
    {
        string ISBN;
        string author;
        string publisher;
        int page;

        /// <summary>
        /// Constructor for Book object.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="price"></param>
        /// <param name="isbn"></param>
        /// <param name="author"></param>
        /// <param name="publisher"></param>
        /// <param name="page"></param>
        /// <param name="stock"></param>
        /// <param name="image"></param>
        public Book(int id, string name, double price, string isbn, string author, string publisher, int page, int stock , byte[] image)
        {
            this.id = id;
            this.name = name;
            this.price = price;
            this.ISBN = isbn;
            this.author = author;
            this.publisher = publisher;
            this.page = page;
            this.stock = stock;
            this.image = image;
        }

        /// <summary>
        /// Prints product information to a DataGridView object.
        /// </summary>
        /// <param name="dgv"></param>
        public override void printProperties(DataGridView dgv)
        {
            dgv.Rows.Add(id,name, author, price.ToString(), publisher, ISBN, page.ToString(), stock, ByteArrayToImage(image));
        }
    }
}
