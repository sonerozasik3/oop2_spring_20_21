﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
/// <summary>
/// This class must be created after getting customer info 
/// because it needs the customer info from customer.cs
/// </summary>
namespace BookStore
{
    class ButtonClickLogger
    {
        private string startupPath;
        private string filename;
        private string filepath;
        private string customerName;
        private string customerId;
        private string newline;


        /// <summary>
        /// Pulls ButtonClickLog_Name.txt from sql db if it exists. Creates one if it doesn't.
        /// </summary>
        public ButtonClickLogger()
        {
            //set customer name and id
            Customer customer = Customer.getInstance();
            customerName = customer.getCustomerName();
            customerId = customer.getCustomerId().ToString();
            //set file path and file name
            startupPath = System.IO.Directory.GetCurrentDirectory();
            this.filename = "ButtonClickLog_" + customerName + ".txt";
            //get users past button log
            filepath = Path.Combine(startupPath, filename);
            newline = "Name".PadRight(15) + "Button Clicked".PadRight(35) + "ClickDate".PadRight(25) +  "\n" ;
            if (!File.Exists(filepath))
            {
                if (!GetLogFileFromSql())
                {
                    File.AppendAllText(filepath, newline);
                }
            }
        }
        /// <summary>
        /// Logs clicked button to ButtonClickLog_Name.txt.
        /// </summary>
        /// <param name="ButtonName">Name of button to log.</param>
        public void LogButton(string ButtonName)
        {
            newline = customerName.PadRight(15) + ButtonName.PadRight(35) + DateTime.Now.ToString().PadRight(25) + "\n";
            File.AppendAllText(filepath, newline);

        }
        /// <summary>
        /// Gets log file from sql.
        /// </summary>
        /// <returns>Return true if successful</returns>
        private bool GetLogFileFromSql()
        {
            SqlConnection connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();
            SqlCommand cmd = new SqlCommand("SELECT Buttonlog FROM Customer WHERE CustomerId = @customerId", connection);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@customerid", customerId);
            SqlDataReader Reader;
            Reader = cmd.ExecuteReader();
            Reader.Read();
            if(Reader.IsDBNull(0) )
            {
                return false;
            }
            byte[] textfilearr = null;
            textfilearr = (byte[])Reader.GetValue(0);
            File.WriteAllBytes(filepath, textfilearr);

            
            connection.Close();
            return true;
        }
        /// <summary>
        /// Upload log file to sql.
        /// </summary>
        public void UploadLogFileToSql()
        {
            SqlConnection connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();
            byte[] textfilearr = File.ReadAllBytes(filepath);
            SqlCommand cmd = new SqlCommand("UPDATE Customer SET Buttonlog = @buttonlog WHERE CustomerId = @customerId", connection);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@customerid", customerId);
            cmd.Parameters.AddWithValue("@buttonlog", textfilearr);

            cmd.ExecuteNonQuery();

            connection.Close();
        }
        /// <summary>
        /// Open log file in notepad instance.
        /// </summary>
        public void OpenLogFileInNotePad()
        {
            Process.Start("notepad.exe", filepath);
        }



    }
}
