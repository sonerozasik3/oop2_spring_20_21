﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
namespace BookStore
{
    /// <summary>
    /// A Singleton Customer Class.Includes functions for logged user.
    /// </summary>
    public class Customer
    {
        static Customer instance;
        /// <summary>
        /// getInstance Function: If instance is null, creates an new Customer
        /// </summary>
        /// <returns>Customer</returns>
        public static Customer getInstance()
        {
            if (instance == null)
            {
                instance = new Customer();
            }
            return instance;
        }

        private int _customerId ;
        private string _name;
        private string email;
        private string address;
        private string username;
        private string password;

        private XmlDocument shoppingcart_xml;
        public XmlDocument Shoppingcart_xml
        {
            get
            {   return shoppingcart_xml;}
        }

        private Customer(){}
        /// <summary>
        /// Sets the customer.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="name"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="address"></param>
        /// <param name="shoppingcart_xml"></param>
        public void SetCustomer(int customerId, string name, string username, string password , string email , string address ,XmlDocument shoppingcart_xml)
        {
            this._customerId = customerId;
            this._name = name;
            this.username = username;
            this.address = address;
            this.email = email;
            this.password = password;
            this.shoppingcart_xml = shoppingcart_xml;
        }

        /// <summary>
        /// Prints customer details to TextBoxes
        /// </summary>
        /// <param name="txtname"></param>
        /// <param name="txtusername"></param>
        /// <param name="txtpass"></param>
        /// <param name="txtaddress"></param>
        /// <param name="txtmail"></param>
        public void printCustomerDetails(TextBox txtname ,TextBox txtusername , TextBox txtpass , TextBox txtaddress, TextBox txtmail)
        {
            txtname.Text = this._name;
            txtusername.Text = this.username;
            txtpass.Text = this.password;
            txtmail.Text = this.email;
            txtaddress.Text = this.address;
        }

        /// <summary>
        /// Saves customer informations to repository.
        /// </summary>
        public void saveCustomer()
        {
            Repository repository = Repository.getInstance();
            repository.updateCustomer();
        }

        /// <summary>
        /// Prints customer purchases to DataGridView.
        /// </summary>
        /// <param name="dgv"></param>
        public void printCustomerPurchases(DataGridView dgv)
        {
            SqlConnection connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();
            SqlCommand command = new SqlCommand("SELECT PurchasedItems FROM Orders WHERE CustomerId LIKE '" + _customerId + "'", connection);
            command.ExecuteNonQuery();

            if(command.ExecuteScalar() == null)
            {
               MessageBox.Show("You don't have any purchased items!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                XmlDocument xdoc = new XmlDocument();
                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Orders WHERE CustomerId LIKE '" + _customerId + "'", connection);

                DataTable table = new DataTable();
                da.Fill(table);
                DataRow row = table.Rows[0];
                dgv.DataSource = table;
                dgv.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                DataGridViewRow dgvrow = dgv.RowTemplate;
                xdoc.LoadXml(row.Field<string>("PurchasedItems"));
                connection.Close();
            }
        }


        public string Email
        {
            get { return email; }
        }

        public string Address
        {
            get { return address; }
        }

        public string Password
        {
            get { return password; }
        }

        public int getCustomerId()
        {
            return _customerId;
        }
        public string getCustomerName()
        {
            return _name;
        }
    }
}
