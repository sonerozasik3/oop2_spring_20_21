﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BookStore
{
    /// <summary>
    /// Store Form
    /// </summary>
    public partial class Form1 : Form , Observer
    {
        Repository repository = Repository.getInstance();
        ShoppingCart shoppingCart = new ShoppingCart();
        ShoppingCartForm sc;
        Ixml ixml = Ixml.getInstance();
        int selected_item_id=-1;
        Subject subject = Subject.getInstance();

        public Form1()
        {
            InitializeComponent();
            Customer customer = Customer.getInstance();
            shoppingCart = ixml.XmlToShoppingCart();
        }

        /// <summary>
        /// Updates the DataGridView for categories
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lb_category_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgv_storefront.Columns.Clear();
            dgv_storefront.EnableHeadersVisualStyles = false;
            dgv_storefront.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(48, 51, 56);
            dgv_storefront.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(228, 228, 232);
            try
            {
                if (lb_category.SelectedItem == "Books")
                {
                    ButtonClickLogger btg = new ButtonClickLogger();
                    btg.LogButton("Form1 Books");
                    dgv_storefront.Columns.Add("id", "id");
                    dgv_storefront.Columns.Add("Name", "Name");
                    dgv_storefront.Columns.Add("Author", "Author");
                    dgv_storefront.Columns.Add("Price", "Price");
                    dgv_storefront.Columns.Add("Publisher", "Publisher");
                    dgv_storefront.Columns.Add("ISBN", "ISBN");
                    dgv_storefront.Columns.Add("Page", "Page");
                    dgv_storefront.Columns.Add("Stock", "Stock");
                    DataGridViewImageColumn imgcolumn = new DataGridViewImageColumn();
                    imgcolumn.Name = "Image";
                    imgcolumn.HeaderText = "Cover";
                    dgv_storefront.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                    dgv_storefront.Columns.Add(imgcolumn);
                    DataGridViewRow dgvrow = dgv_storefront.RowTemplate;
                    dgvrow.Height = 120;
                    imgcolumn.ImageLayout = DataGridViewImageCellLayout.Stretch;
                    repository.GetBooks().ForEach(b => { b.printProperties(dgv_storefront); });
                    dgv_storefront.Columns[0].Visible = false;
                }
                else if (lb_category.Text == "Magazine")
                {
                    ButtonClickLogger btg = new ButtonClickLogger();
                    btg.LogButton("Form1 Magazine");
                    dgv_storefront.Columns.Add("id", "id");
                    dgv_storefront.Columns.Add("Name", "Name");
                    dgv_storefront.Columns.Add("Price", "Price");
                    dgv_storefront.Columns.Add("Type", "Type");
                    dgv_storefront.Columns.Add("Issue", "Issue");
                    dgv_storefront.Columns.Add("Stock", "Stock");
                    DataGridViewImageColumn imgcolumn = new DataGridViewImageColumn();
                    imgcolumn.Name = "Image";
                    imgcolumn.HeaderText = "Cover";
                    dgv_storefront.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                    dgv_storefront.Columns.Add(imgcolumn);
                    DataGridViewRow dgvrow = dgv_storefront.RowTemplate;
                    dgvrow.Height = 120;
                    imgcolumn.ImageLayout = DataGridViewImageCellLayout.Stretch;
                    repository.GetMagazines().ForEach(m => { m.printProperties(dgv_storefront); });
                    dgv_storefront.Columns[0].Visible = false;

                }
                else if (lb_category.Text == "MusicCD")
                {
                    ButtonClickLogger btg = new ButtonClickLogger();
                    btg.LogButton("Form1 MusicCD");
                    dgv_storefront.Columns.Add("id", "id");
                    dgv_storefront.Columns.Add("Name", "Name");
                    dgv_storefront.Columns.Add("Singer", "Singer");
                    dgv_storefront.Columns.Add("Price", "Price");
                    dgv_storefront.Columns.Add("Type", "Type");
                    dgv_storefront.Columns.Add("Stock", "Stock");
                    DataGridViewImageColumn imgcolumn = new DataGridViewImageColumn();
                    imgcolumn.Name = "Image";
                    imgcolumn.HeaderText = "Cover";
                    dgv_storefront.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                    dgv_storefront.Columns.Add(imgcolumn);
                    DataGridViewRow dgvrow = dgv_storefront.RowTemplate;
                    dgvrow.Height = 120;
                    imgcolumn.ImageLayout = DataGridViewImageCellLayout.Stretch;
                    repository.GetMusicCDs().ForEach(c => { c.printProperties(dgv_storefront); });
                    dgv_storefront.Columns[0].Visible = false;

                }
            }
            catch(Exception)
            {
                
            }                   

        }

        private void btnAddCart_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Form1 Add To Cart");
            if (selected_item_id == -1)
            {
                MessageBox.Show("Choose an Item!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
               
                if (repository.getProduct(selected_item_id).Stock >= int.Parse(textBox1.Text) && int.Parse(textBox1.Text) > 0)
                {
                    ItemToPurchase item = new ItemToPurchase(repository.getProduct(selected_item_id), int.Parse(textBox1.Text));
                    shoppingCart.AddProduct(item);
                    textBox1.Text = "";
                    subject.Attach(this);
                    subject.Notify();
                }
                else
                {
                    MessageBox.Show("You can't buy this amount of item!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void dgv_storefront_SelectionChanged(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Form1 Select Item");
            if (dgv_storefront.SelectedCells.Count > 0)
            {
                textBox1.Text = "1";
                int selectedrowindex = dgv_storefront.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dgv_storefront.Rows[selectedrowindex];
                selected_item_id = Convert.ToInt32(selectedRow.Cells["id"].Value);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Form1 Go To Shopping Cart");
            sc = new ShoppingCartForm(shoppingCart);
            sc.Show();
            
        }


        public void update()
        {
            dgv_storefront.Rows.Clear();
            if (lb_category.SelectedItem == "Books")
            {
                repository.GetBooks().ForEach(b => { b.printProperties(dgv_storefront); });
            }
            else if (lb_category.Text == "Magazine")
            {
        
                repository.GetMagazines().ForEach(m => { m.printProperties(dgv_storefront); });

            }
            else if (lb_category.Text == "MusicCD")
            {
    
                repository.GetMusicCDs().ForEach(c => { c.printProperties(dgv_storefront); });


            }

        }

        private void btn_signout_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Form1 Sign Out");
            btg.UploadLogFileToSql();
            Login login = new Login();
            login.Show();
            this.Close();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            subject.Detach(this);
        }

        private void btn_profile_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Form1 Show Profile");
            Profile profile = new Profile();
            profile.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Form1 Minimize");
            WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Form1 Close App");
            btg.UploadLogFileToSql();
            Close();
            Application.Exit();
        }

    }
}
