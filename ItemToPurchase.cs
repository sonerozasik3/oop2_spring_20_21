﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore
{
    public class ItemToPurchase
    {
        private Product _product;
        private int quantity;

        public ItemToPurchase(Product product,int quantity)
        {
            this._product = product;
            this.quantity = quantity;
        }

        public Product product
        {
            get { return _product; }
            set { _product = value; }
        }

        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
    }
}
