﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BookStore
{
    class Ixml
    {
        Customer customer = Customer.getInstance();
        static Ixml instance;
        XmlWriterSettings xmlWriterSettings;
        XmlDocument xdoc;
        public static Ixml getInstance()
        {
            if (instance == null)
            {
                instance = new Ixml();
            }
            return instance;
        }
        private Ixml()
        {
            this.xdoc = new XmlDocument();
            this.xmlWriterSettings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                NewLineChars = "\r\n",
                NewLineOnAttributes = true
            }; 
        }

        

        public byte[] AlternativeParseShoppingCartToXmlReturnByteArray(ShoppingCart shoppingcart)
        {
            XmlDocument xdoc = new XmlDocument();
            XmlNode rootNode = xdoc.CreateElement("ShoppingCart");
            XmlAttribute Unique_Item_CountAttribute = xdoc.CreateAttribute("Unique_Item_Count");
            Unique_Item_CountAttribute.Value = shoppingcart.GetShoppingCartContents().Count.ToString();
            rootNode.Attributes.Append(Unique_Item_CountAttribute);
            xdoc.AppendChild(rootNode);

            XmlNode customerNode = xdoc.CreateElement("Customer");
            XmlAttribute customerIdAttribute = xdoc.CreateAttribute("ID");
            customerNode.InnerText = customer.getCustomerName();
            customerIdAttribute.Value = customer.getCustomerId().ToString();
            customerNode.Attributes.Append(customerIdAttribute);
            rootNode.AppendChild(customerNode);

            for (int i = 0; i < shoppingcart.GetShoppingCartContents().Count; i++)
            {
                XmlNode boughtItemNode = xdoc.CreateElement("Item_In_Cart");
                boughtItemNode.InnerText = shoppingcart.GetShoppingCartContents()[i].product.Name.ToString();

                XmlAttribute itemIdAttribute = xdoc.CreateAttribute("ID");
                itemIdAttribute.Value = shoppingcart.GetShoppingCartContents()[i].product.Id.ToString();
                boughtItemNode.Attributes.Append(itemIdAttribute);

                XmlAttribute itemAmountAttribute = xdoc.CreateAttribute("Amount");
                itemAmountAttribute.Value = shoppingcart.GetShoppingCartContents()[i].Quantity.ToString();
                boughtItemNode.Attributes.Append(itemAmountAttribute);

                rootNode.AppendChild(boughtItemNode);

            }
            xdoc.Save("shoppingcart.xml");
            return XmlDocumentToByteArray(xdoc);
        }


        public XmlDocument ParseOrderToXml(ShoppingCart shoppingcart, Customer customer)
        {
            return null;

        }

        public ShoppingCart XmlToShoppingCart()
        {
            try
            {
                Repository rep = Repository.getInstance();
                customer.Shoppingcart_xml.Save("XmlToShoppingCart().xml");
                ItemToPurchase itp = null;

                XmlDocument xdoc = new XmlDocument();
                xdoc = customer.Shoppingcart_xml;
                XmlElement root = xdoc.DocumentElement;
                int unique_item_quantity = int.Parse(root.Attributes["Unique_Item_Count"].Value);
                List<ItemToPurchase> itp_list = new List<ItemToPurchase>();

                foreach (XmlNode child in root.SelectNodes("Item_In_Cart"))
                {
                    itp = new ItemToPurchase(rep.getProduct(int.Parse(child.Attributes["ID"].Value)), int.Parse(child.Attributes["Amount"].Value));
                    itp_list.Add(itp);
                }

                ShoppingCart sp = new ShoppingCart(customer.getCustomerId(), itp_list);
                return sp;
            }
            catch(Exception)
            {

            }
            return null;
        }
        public List<ItemToPurchase> XMLtoProductList(XmlDocument xml)
        {
            Repository rep = Repository.getInstance();
            customer.Shoppingcart_xml.Save("XmlToShoppingCart().xml");
            ItemToPurchase itp = null;

            XmlDocument xdoc = new XmlDocument();
            xdoc = xml;
            XmlElement root = xdoc.DocumentElement;
            int unique_item_quantity = int.Parse(root.Attributes["Unique_Item_Count"].Value);
            List<ItemToPurchase> itp_list = new List<ItemToPurchase>();

            foreach (XmlNode child in root.SelectNodes("Bought_Item"))
            {
                itp = new ItemToPurchase(rep.getProduct(int.Parse(child.Attributes["ID"].Value)), int.Parse(child.Attributes["Amount"].Value));
                itp_list.Add(itp);
            }

            return itp_list;
            //Repository rep = Repository.getInstance();
            //xml.Save("XmlToDataTable().xml");
            //ItemToPurchase itp = null;


            //XmlElement root = xml.DocumentElement;
            //int unique_item_quantity = int.Parse(root.Attributes["Unique_Item_Count"].Value);
            //List<ItemToPurchase> itp_list = new List<ItemToPurchase>();

            //foreach (XmlNode child in root.SelectNodes("Bought_Item"))
            //{
            //    itp = new ItemToPurchase(rep.getProduct(int.Parse(child.Attributes["ID"].Value)), int.Parse(child.Attributes["Amount"].Value));
            //    itp_list.Add(itp);
            //}

            //return itp_list;
        }

        public DataTable XMLtoDataTable(XmlDocument xml)
        {
            Repository rep = Repository.getInstance();
            xml.Save("XmlToDataTable().xml");
            ItemToPurchase itp = null;


            XmlElement root = xml.DocumentElement;
            int unique_item_quantity = int.Parse(root.Attributes["Unique_Item_Count"].Value);
            List<ItemToPurchase> itp_list = new List<ItemToPurchase>();

            foreach (XmlNode child in root.SelectNodes("Bought_Item"))
            {
                itp = new ItemToPurchase(rep.getProduct(int.Parse(child.Attributes["ID"].Value)), int.Parse(child.Attributes["Amount"].Value));
                itp_list.Add(itp);
            }

            DataTable table = new DataTable();
            table.Columns.Add("Product Name", typeof(string));
            table.Columns.Add("Product Price", typeof(double));
            table.Columns.Add("Product Amount", typeof(int));


            for (int i = 0; i < itp_list.Count; i++) {
                DataRow row = null;
                row.ItemArray[0] = itp_list[i].product.Name;
                row.ItemArray[1] = itp_list[i].product.Price;
                row.ItemArray[2] = itp_list[i].Quantity;
                table.Rows.Add();
              } 
            return table;
        }

        public byte[] XmlDocumentToByteArray(XmlDocument xdoc)
        {
            byte[] XmlByteArr = Encoding.UTF8.GetBytes(xdoc.OuterXml);
            return XmlByteArr;
        }

        public XmlDocument ByteArrayToXmlDocument(byte[] XmlByteArr)
        {
            XmlDocument xdoc = new XmlDocument();
            string xml_string = Encoding.UTF8.GetString(XmlByteArr);
            xdoc.Load(xml_string);
            xdoc.Save("xml_from_sql.xml"); //this is for debugging delete later to-do
            return xdoc;

        }

        //credits: https://www.c-sharpcorner.com/blogs/converting-dataset-into-byte-array1
        public byte[] ConvertDataSetToByteArray(DataSet dataSet)
        {
            byte[] binaryDataResult = null;
            using (MemoryStream memStream = new MemoryStream())
            {
                BinaryFormatter brFormatter = new BinaryFormatter();
                dataSet.RemotingFormat = SerializationFormat.Binary;
                brFormatter.Serialize(memStream, dataSet);
                binaryDataResult = memStream.ToArray();
            }
            return binaryDataResult;
        }




    }
}
