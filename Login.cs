﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
namespace BookStore
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

  
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_login_Click_1(object sender, EventArgs e)
        {
            lbl_status.Text = "Logging in...";
            if (userTb.Text == "")
            {
                lbl_status.Text = "Missing Information!\nPlease enter your username!";
            }
            else if (passTb.Text == "")
            {
                lbl_status.Text = "Missing Information!\nPlease enter your password!";
            }
            Repository repository = Repository.getInstance();
            int check = repository.checkUserInformation(userTb.Text, passTb.Text);

            if (check == -1)
            {
                lbl_status.Text = "No such user exist!";
                userTb.Text = "";
                passTb.Text = "";
            }
            else if (check == 0)
            {
                lbl_status.Text = "Wrong password!";
                userTb.Text = "";
                passTb.Text = "";
            }
            else if (check == 1)
            {
                lbl_status.Text = "Successful Login!";
                string userNameTemp = userTb.Text;
                userTb.Text = "";
                passTb.Text = "";

                Application.DoEvents();
                Thread.Sleep(500);
                repository.GetCustomerInfoFromSql(userNameTemp);
                ButtonClickLogger btg = new ButtonClickLogger();
                btg.LogButton("Login");
                Form1 form1 = new Form1();
                form1.Show();
                this.Hide();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            string startupPath = System.IO.Directory.GetCurrentDirectory();
            string filename = "Book_Store.png";
            string filepath = Path.Combine(startupPath, filename);
            pictureBox1.Image = Image.FromFile(filepath);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SignUp signUp = new SignUp();
            signUp.Show();
            this.Hide();
              
        }
    }
}
