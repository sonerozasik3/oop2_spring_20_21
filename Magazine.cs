﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;


namespace BookStore
{
    /// <summary>
    /// Magazine product class. Inherited from Product class.
    /// </summary>
    class Magazine: Product
    {
        string issue;
        string type;
        /// <summary>
        /// Constructor for Magazine object.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="price"></param>
        /// <param name="type"></param>
        /// <param name="issue"></param>
        /// <param name="stock"></param>
        /// <param name="image"></param>
        public Magazine(int id, string name, double price , string type , string issue , int stock , byte[] image)
        {
            this.id = id;
            this.name = name;
            this.price = price;
            this.type = type;
            this.issue = issue;
            this.stock = stock;
            this.image = image;
        }
        /// <summary>
        /// Prints product information to a DataGridView object.
        /// </summary>
        /// <param name="dgv"></param>
        public override void printProperties(DataGridView dgv)
        {

            dgv.Rows.Add(id,name, price.ToString(), type, issue, stock.ToString(), ByteArrayToImage(image));


        }
    }
}
