﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;


namespace BookStore
{
    /// <summary>
    /// MusicCD product class. Inherited from Product class.
    /// </summary>
    class MusicCD : Product
    {
        string singer;
        string type;
        /// <summary>
        /// Constructor for MusicCD object.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="price"></param>
        /// <param name="type"></param>
        /// <param name="singer"></param>
        /// <param name="stock"></param>
        /// <param name="image"></param>
        public MusicCD(int id, string name, double price , string type , string singer , int stock , byte[] image)
        {
            this.id = id;
            this.name = name;
            this.price = price;
            this.type = type;
            this.singer = singer;
            this.stock = stock;
            this.image = image;
        }
        /// <summary>
        /// Prints product information to a DataGridView object.
        /// </summary>
        /// <param name="dgv"></param>
        public override void printProperties(DataGridView dgv)
        {
            
            dgv.Rows.Add(id,name, singer, price.ToString(), type, stock, ByteArrayToImage(image));


        }

    }
}
