﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class PaymentForm : Form
    {
        ShoppingCart shoppingCart;
        public PaymentForm(ShoppingCart sc)
        {
            InitializeComponent();
            shoppingCart = sc;
        }

        private void PaymentForm_Load(object sender, EventArgs e)
        {
            txt_totalprice.Text = shoppingCart.getTotalPrice().ToString();
            textBox1.ReadOnly = true;
            textBox3.ReadOnly = true;
            textBox2.ReadOnly = true;
        }

        private void btn_complete_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Payment Complete Order");
            if(checkInformations())
            {
                Repository repository = Repository.getInstance();
                repository.CompletePurchase(shoppingCart, cB_payment.Text);
                shoppingCart.placeOrder();
                repository.UploadShoppingCartToDB(shoppingCart);
                this.Hide();
                MessageBox.Show("Purchase is Succesfull!", "Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                shoppingCart.sendInvoiceByEmail();
            }
        }

        private void cB_payment_SelectedIndexChanged(object sender, EventArgs e)
        {
          if(cB_payment.Text == "Card")
            {
                textBox1.ReadOnly = false;
                textBox3.ReadOnly = false;
                textBox2.ReadOnly = false;

            }
            else
            {
                textBox1.ReadOnly = true;
                textBox3.ReadOnly = true;
                textBox2.ReadOnly = true;

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Payment Back");
            this.Hide();
        }

        public bool checkInformations()
        {
            if(cB_payment.Text == "Card")
            {
                if (textBox1.Text.Length != 16)
                {
                    MessageBox.Show("Invalid card number.It must be 16 digits!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (textBox3.Text.Length != 3)
                {
                    MessageBox.Show("Invalid security number.It must be 3 digits!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            return true;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txt_totalprice_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("PaymentForm Minimize");
            WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("PaymentForm Close Window");
            this.Close();
        }
    }
}
