﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace BookStore
{
    /// <summary>
    /// Product class. Real products inherit this class.
    /// </summary>
    public abstract class Product
    {
        protected int id;
        protected string name;
        protected double price;
        protected int stock;
        protected byte[] image;

        public int Id
        {
            get { return id; }
            set { this.id = value; }
        }

        public string Name
        {
            get { return name; }
            set { this.name = value; }
        }
        public double Price
        {
            get { return price; }
            set { this.price = value; }
        }

        public int Stock
        {
            get { return stock; }
            set { this.stock = value; }
        }
        public byte[] IMG
        {
            get { return image; }
            set { this.image = value; }
        }
        /// <summary>
        /// Virtual method
        /// <param name="dgv"></param>
        public virtual void printProperties(DataGridView dgv)
        {



        }
        /// <summary>
        /// Parses an byte array to an Image object.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>Image object</returns>
        public Image ByteArrayToImage(byte[] data)
        {
            MemoryStream ms = new MemoryStream(data);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }



    }
}
