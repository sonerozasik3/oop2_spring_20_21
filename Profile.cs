﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class Profile : Form
    {
        Form1 form1 = new Form1();
        Customer customer = Customer.getInstance();
        public Profile()
        {
            InitializeComponent();
        }

        private void Profile_Load(object sender, EventArgs e)
        {
            customer.printCustomerDetails(txt_name,txt_username,txt_password ,txt_address , txt_email);
        }

        private void btn_changeprofile_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Profile Update Informations");
            customer.SetCustomer(customer.getCustomerId(),txt_name.Text, txt_username.Text, txt_password.Text, txt_email.Text, txt_address.Text, customer.Shoppingcart_xml);
            customer.saveCustomer();
            MessageBox.Show("Your informations has been updated succesfully!", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
            form1.Show();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Orders Back");
            this.Hide();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Profile Show Orders");
            PurchasedItems pı = new PurchasedItems();
            pı.Show();
            this.Hide();
            
        }

        private void btn_viewbuttonlog_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Profile View Button Log");
            btg.OpenLogFileInNotePad();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Profile Upload Button Log");
            btg.UploadLogFileToSql();
        }

        private void txt_email_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Profile Close Window");
            this.Close();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Profile Minimize");
            WindowState = FormWindowState.Minimized;
        }
    }
}
