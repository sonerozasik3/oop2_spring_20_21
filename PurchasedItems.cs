﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace BookStore
{
    public partial class PurchasedItems : Form
    {
        Profile profile = new Profile();
        public PurchasedItems()
        {
            InitializeComponent();
        }


        private void btn_back_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Orders Back");
            this.Hide();
            profile.Show();
        }

        private void PurchasedItems_Load(object sender, EventArgs e)
        {
            Customer customer = Customer.getInstance();
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(48, 51, 56);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(228, 228, 232);

            customer.printCustomerPurchases(dataGridView1);
            if(dataGridView1.Rows.Count != 0)
            {
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Columns[6].Visible = false;
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Customer customer = Customer.getInstance();
            Ixml ixml = Ixml.getInstance();
            XmlDocument xdoc = new XmlDocument();

            xdoc.LoadXml(dataGridView1.SelectedCells[0].OwningRow.Cells[6].Value.ToString());
            PurchasedItemsList piList = new PurchasedItemsList(ixml.XMLtoProductList(xdoc));
            piList.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("PurchasedItems Minimize");
            WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("PurchasedItem Close Window");
            this.Close();
            profile.Show();
        }
    }
}