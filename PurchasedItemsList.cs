﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
namespace BookStore
{
    public partial class PurchasedItemsList : Form
    {
        List<ItemToPurchase> list;
        public PurchasedItemsList(List<ItemToPurchase> list)
        {
            InitializeComponent();
            this.list = list;
        }

        private void PurchasedItemsList_Load(object sender, EventArgs e)
        {
            Ixml ixml = Ixml.getInstance();
            dgv.EnableHeadersVisualStyles = false;
            dgv.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(48, 51, 56);
            dgv.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(228, 228, 232);
            dgv.Columns.Add("id", "id");
            dgv.Columns.Add("Name", "Name");
            dgv.Columns.Add("Price", "Price");
            dgv.Columns.Add("Amount", "Amount");


            DataGridViewImageColumn imgcolumn = new DataGridViewImageColumn();
            imgcolumn.Name = "Image";
            imgcolumn.HeaderText = "Image";
            dgv.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            dgv.Columns.Add(imgcolumn);
            DataGridViewRow dgvrow = dgv.RowTemplate;
            dgvrow.Height = 90;
            imgcolumn.ImageLayout = DataGridViewImageCellLayout.Stretch;

            for (int i = 0; i < list.Count; i++)
            {
                dgv.Rows.Add(list[i].product.Id, list[i].product.Name, list[i].product.Price, list[i].Quantity, list[i].product.ByteArrayToImage(list[i].product.IMG));
            }
            dgv.Columns[0].Visible = false;
        }


        private void button4_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("PurchasedItemsList Close Window");
            PurchasedItems pcı = new PurchasedItems();
            this.Close();
            pcı.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("PurchasedItemsList Minimize");
            WindowState = FormWindowState.Minimized;
        }

    }
}
