﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
namespace BookStore
{
    public class Repository:Observer
    {
        static Repository instance;
        Customer customer = Customer.getInstance();
        Ixml ixml = Ixml.getInstance();

        public static Repository getInstance()
        {
            if(instance == null)
            {
                instance = new Repository();
            }
            return instance;
        }

        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter da;

        List<Product> products=new List<Product>();

        private Repository()
        {
            updateLocalProducts();
        }
        public List<Product> GetProducts()
        {
            return products;
        }

        public List<Product> GetBooks()
        {
            List<Product> books = new List<Product>();

            for (int i = 0; i < products.Count; i++)
            {
                if (getTypeCode(products[i].Id) == 10)
                {
                    books.Add(products[i]);
                }
            }
            return books;
        }
        public List<Product> GetMagazines()
        {
            List<Product> magazines = new List<Product>();
            for (int i = 0; i < products.Count; i++)
            {
                if (getTypeCode(products[i].Id) == 11)
                {
                    magazines.Add(products[i]);
                }
            }
            return magazines;
        }

        public List<Product> GetMusicCDs()
        {
            List<Product> MusicCds = new List<Product>();
            for (int i = 0; i < products.Count; i++)
            {
                if (getTypeCode(products[i].Id) == 12)
                {
                    MusicCds.Add(products[i]);
                }
            }
            return MusicCds;
        }

        public void updateLocalProducts()
        {
            products.Clear();
            connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();



            da = new SqlDataAdapter("SELECT *FROM Book", connection);
            DataTable table = new DataTable();
            da.Fill(table);
            for(int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                Book book = new Book(row.Field<int>("id"), row.Field<string>("name"), (double)row.Field<decimal>("price") , row.Field<string>("ISBN") , row.Field<string>("author"),row.Field<string>("publisher"),row.Field<int>("page"),row.Field<int>("stock") , row.Field<byte[]>("image"));//to do
                products.Add(book);
            }



            da = new SqlDataAdapter("SELECT *FROM Magazine", connection);
            table.Clear();
            da.Fill(table);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                Magazine magazine = new Magazine(row.Field<int>("id"), row.Field<string>("name"), (double)row.Field<decimal>("price"), row.Field<string>("type"),row.Field<string>("issue"),row.Field<int>("stock"),row.Field<byte[]>("image"));//to do
                products.Add(magazine);
            }


            da = new SqlDataAdapter("SELECT *FROM MusicCD", connection);
            table.Clear();
            da.Fill(table);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                MusicCD musicCd = new MusicCD(row.Field<int>("id"), row.Field<string>("name"), (double)row.Field<decimal>("price"),row.Field<string>("type"),row.Field<string>("singer"),row.Field<int>("stock"),row.Field<byte[]>("image"));//to do
                products.Add(musicCd);
            }

        }

        public void UploadShoppingCartToDB(ShoppingCart shoppingcart)
        {

            connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();

            byte[] ShoppingCartByteArr = ixml.AlternativeParseShoppingCartToXmlReturnByteArray(shoppingcart);
            string customerId = customer.getCustomerId().ToString();

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            SqlCommand cmd = new SqlCommand("UPDATE Customer SET ShoppingCart = @ShoppingCartByteArr WHERE CustomerId = @customerId", connection);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ShoppingCartByteArr", ShoppingCartByteArr);
            cmd.Parameters.AddWithValue("@customerid", customerId);

            cmd.ExecuteNonQuery();

            connection.Close();
        }


        public void UploadRepositoryToDB()
        {
            connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();
            SqlCommand cmd;
            
            for(int i = 0; i < products.Count;i++)
            {
                int Stock = products[i].Stock;
                int productId = products[i].Id;
                if (getTypeCode(products[i].Id) == 10)
                {
                    cmd = new SqlCommand("UPDATE Book SET stock = @Stock WHERE id = @productId", connection);
                    cmd.Parameters.AddWithValue("@Stock", Stock);
                    cmd.Parameters.AddWithValue("@productId", productId);
                    cmd.ExecuteNonQuery();
                }
                else if (getTypeCode(products[i].Id) == 11)
                {
                    cmd = new SqlCommand("UPDATE Magazine SET stock = @Stock WHERE id = @productId", connection);
                    cmd.Parameters.AddWithValue("@Stock", Stock);
                    cmd.Parameters.AddWithValue("@productId", productId);
                    cmd.ExecuteNonQuery();
                }
                else if (getTypeCode(products[i].Id) == 12)
                {
                    cmd = new SqlCommand("UPDATE MusicCD SET stock = @Stock WHERE id = @productId", connection);
                    cmd.Parameters.AddWithValue("@Stock", Stock);
                    cmd.Parameters.AddWithValue("@productId", productId);
                    cmd.ExecuteNonQuery();
                }


            }

            connection.Close();
        }
        public void GetCustomerInfoFromSql(string username)
        {
            connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();
            da = new SqlDataAdapter("SELECT * FROM Customer WHERE Username LIKE '" + username + "'", connection);

            DataTable table = new DataTable();
            da.Fill(table);
            DataRow row = table.Rows[0];
            XmlDocument xdoc = new XmlDocument();
            if (row.Field<string>("ShoppingCart") == null)
            {
                XmlDocument emptyxml = new XmlDocument();
                XmlNode rootNode = emptyxml.CreateElement("ShoppingCart");
                XmlAttribute Unique_Item_CountAttribute = emptyxml.CreateAttribute("Unique_Item_Count");
                Unique_Item_CountAttribute.Value = "0";
                rootNode.Attributes.Append(Unique_Item_CountAttribute);
                emptyxml.AppendChild(rootNode);
                XmlNode customerNode = emptyxml.CreateElement("Customer");
                XmlAttribute customerIdAttribute = emptyxml.CreateAttribute("ID");
                customerNode.InnerText = customer.getCustomerName();
                customerIdAttribute.Value = customer.getCustomerId().ToString();
                customerNode.Attributes.Append(customerIdAttribute);
                rootNode.AppendChild(customerNode);
                customer.SetCustomer(row.Field<int>("customerId"), row.Field<string>("Name"), row.Field<string>("Username"), row.Field<string>("Password"), row.Field<string>("Email"), row.Field<string>("Address"),  emptyxml);
                ixml.XmlToShoppingCart();
            }
            else
            {
                xdoc.LoadXml(row.Field<string>("ShoppingCart"));
                customer.SetCustomer(row.Field<int>("customerId"), row.Field<string>("Name"), row.Field<string>("Username"), row.Field<string>("Password"), row.Field<string>("Email"), row.Field<string>("Address"), xdoc);
                ixml.XmlToShoppingCart();
            }
            connection.Close();

        }

        public int checkUserInformation(string username ,string password)
        {
            connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();
            command = new SqlCommand("SELECT Password FROM Customer WHERE Username LIKE '" + username + "'", connection);
            command.ExecuteNonQuery();

            if (command.ExecuteScalar() == null)
            {
                return -1;
            }
            else
            {
                if (command.ExecuteScalar().ToString() != password)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            connection.Close();
        }
        public bool checkUsernameExist(string username)
        {
            connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();
            command = new SqlCommand("SELECT Password FROM Customer WHERE Username LIKE '" + username + "'", connection);
            command.ExecuteNonQuery();

            if (command.ExecuteScalar() == null)
            {
                connection.Close();

                return false;

            }
            else
            {
                connection.Close();

                return true;

            }
        }

        public void addCustomer(string Name, string Email, string Address, string Username, string Password)
        {
            connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();
            command = new SqlCommand("SELECT CustomerId FROM Customer WHERE CustomerId=(SELECT MAX(CustomerId) FROM Customer)", connection);
            command.ExecuteNonQuery();
            var a = (Int32)command.ExecuteScalar();

            SqlCommand cmd = new SqlCommand("INSERT INTO Customer (CustomerId, Name, Email, Address, Username, Password) VALUES (@CI, @Name, @Email, @Address , @Username, @Password)", connection);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CI", a + 1);
            cmd.Parameters.AddWithValue("@Name", Name);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Address", Address);
            cmd.Parameters.AddWithValue("@Username", Username);
            cmd.Parameters.AddWithValue("@Password", Password);
            cmd.ExecuteNonQuery();
            connection.Close();
        }
        public void updateCustomer()
        {
            connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();
            SqlCommand cmd = new SqlCommand("UPDATE Customer SET Email = @Email WHERE CustomerID = @id", connection);
            cmd.Parameters.AddWithValue("@Email", customer.Email);
            cmd.Parameters.AddWithValue("@id", customer.getCustomerId() );
            cmd.ExecuteNonQuery();

            cmd = new SqlCommand("UPDATE Customer SET Address = @Address WHERE CustomerID = @id", connection);
            cmd.Parameters.AddWithValue("@Address", customer.Address);
            cmd.Parameters.AddWithValue("@id", customer.getCustomerId());
            cmd.ExecuteNonQuery();

            cmd = new SqlCommand("UPDATE Customer SET Password = @Password WHERE CustomerID = @id", connection);
            cmd.Parameters.AddWithValue("@Password", customer.Password);
            cmd.Parameters.AddWithValue("@id", customer.getCustomerId());
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public void CompletePurchase(ShoppingCart shoppingcart,string paymentType)
        {
          
            XmlDocument xdoc = new XmlDocument();
            XmlNode rootNode = xdoc.CreateElement("PurchasedItems");
            XmlAttribute Unique_Item_CountAttribute = xdoc.CreateAttribute("Unique_Item_Count");
            Unique_Item_CountAttribute.Value = shoppingcart.GetShoppingCartContents().Count.ToString();
            rootNode.Attributes.Append(Unique_Item_CountAttribute);
            xdoc.AppendChild(rootNode);

            XmlNode customerNode = xdoc.CreateElement("Customer");
            XmlAttribute customerIdAttribute = xdoc.CreateAttribute("ID");
            customerNode.InnerText = customer.getCustomerName();
            customerIdAttribute.Value = customer.getCustomerId().ToString();
            customerNode.Attributes.Append(customerIdAttribute);
            rootNode.AppendChild(customerNode);

            for (int i = 0; i < shoppingcart.GetShoppingCartContents().Count; i++)
            {
                XmlNode boughtItemNode = xdoc.CreateElement("Bought_Item");
                boughtItemNode.InnerText = shoppingcart.GetShoppingCartContents()[i].product.Name.ToString();

                XmlAttribute itemIdAttribute = xdoc.CreateAttribute("ID");
                itemIdAttribute.Value = shoppingcart.GetShoppingCartContents()[i].product.Id.ToString();
                boughtItemNode.Attributes.Append(itemIdAttribute);

                XmlAttribute itemAmountAttribute = xdoc.CreateAttribute("Amount");
                itemAmountAttribute.Value = shoppingcart.GetShoppingCartContents()[i].Quantity.ToString();
                boughtItemNode.Attributes.Append(itemAmountAttribute);

                rootNode.AppendChild(boughtItemNode);

            }

            byte[] byteArray = ixml.XmlDocumentToByteArray(xdoc);
            connection = new SqlConnection("Data Source=SQL5097.site4now.net;Initial Catalog=db_a753a6_bookstore;User Id=db_a753a6_bookstore_admin;Password=BookStore123");
            connection.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO Orders (CustomerId, TotalPrice, Date, PaymentType, Status, PurchasedItems) VALUES (@CI, @TP, @Date, @PT , @Status, @PI)",connection);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CI", customer.getCustomerId());
            cmd.Parameters.AddWithValue("@TP", shoppingcart.getTotalPrice());
            cmd.Parameters.AddWithValue("@Date", System.DateTime.Now);
            cmd.Parameters.AddWithValue("@PT", paymentType);
            cmd.Parameters.AddWithValue("@Status", "In Procces");
            cmd.Parameters.AddWithValue("@PI", byteArray);

            //  cmd.Parameters.AddWithValue("@OI", OrderId);
            cmd.ExecuteNonQuery();
            connection.Close();
        }
        public void update()
        {
            UploadRepositoryToDB();
        }

        private  int getTypeCode(int id)
        {
            while (id > 99)
            {
                id /= 10;
            }
            return id;
        }

        public Product getProduct(int id)
        {
            for(int i = 0; i < products.Count; i++)
            {
                if (products[i].Id == id)
                    return products[i];
            }
            return null;
        }
    }
}
