﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    /// <summary>
    /// ShoppingCart class. This class has methods for basic shopping cart operations.
    /// </summary>
    public class ShoppingCart
    {
        private int CustomerId;
        List<ItemToPurchase> items =new List<ItemToPurchase>();
        Subject subject = Subject.getInstance();
        Repository repository = Repository.getInstance();
        

        /// <summary>
        /// Constructor for ShoppingCart.
        /// </summary>
        /// <param name="CustomerId"></param>
        public ShoppingCart(int CustomerId , List<ItemToPurchase> temp)
        {
            this.items = temp;
            this.CustomerId = CustomerId;
        }
        /// <summary>
        /// Constructor for ShoppingCart.
        /// </summary>
        public ShoppingCart()
        {}
        /// <summary>
        /// Prints items in shopping cart in a DataGridView object.
        /// </summary>
        /// <param name="dgv"></param>
        public void PrintProducts(DataGridView dgv)
        {
            for (int i=0;i<items.Count;i++)
            {
                dgv.Rows.Add(items[i].product.Id, items[i].product.Name, items[i].product.Price, items[i].Quantity, items[i].product.ByteArrayToImage(items[i].product.IMG));
            }
        }

        /// <summary>
        /// Adds ItemToPurchase object to ShoppingCart list. 
        /// </summary>
        /// <param name="item"></param>
        public void AddProduct(ItemToPurchase item)
        {
            for(int i = 0; i < items.Count; i++)
            {
                if (items[i].product.Id == item.product.Id)
                {
                    items[i].Quantity += item.Quantity;
                    return;
                }
            }
            items.Add(item);

        }
        /// <summary>
        /// Remove Product from ShoppingCart list. 
        /// </summary>
        /// <param name="index"></param>
        public void RemoveProduct(int index)
        {
            items.RemoveAt(index);
        }
        /// <summary>
        /// Sets quantity of product in shopping cart.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="quantity"></param>
        public void setQuantity(int id, int quantity)
        {
            for(int i = 0; i < items.Count; i++)
            {
                if (items[i].product.Id == id)
                {
                    items[i].Quantity = quantity;
                    return;
                }
            }

        }
        /// <summary>
        /// Gets specified Product from shopping cart
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Item wanted: Product</returns>
        public Product getProduct(int id)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].product.Id == id)
                {
                    return items[i].product;
                }
            }
            return null;
        }
        /// <summary>
        /// Gets specified items amount from shoppingcart
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Item amount: int</returns>
        public int getQuantity(int id)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].product.Id == id)
                {
                    return items[i].Quantity;
                }
            }
            return 0;
        }
        /// <summary>
        /// Place order.
        /// </summary>
        public void placeOrder()
        {
            for (int i = 0; i < items.Count; i++)
            {
                items[i].product.Stock -= items[i].Quantity;
            }
            subject.Attach(repository);
            subject.Notify();
            subject.Detach(repository);
            items.Clear();
        }
        public void cancelOrder()
        {

        }

        public void sendInvoiceBySMS()
        {
            MessageBox.Show("Invoice sent by SMS.");

        }

        public void sendInvoiceByEmail()
        {
            Customer customer = Customer.getInstance();
            MessageBox.Show("Invoice sent to " + customer.Email +" .","Succesfully",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
        public List<ItemToPurchase> GetShoppingCartContents()
        {
            return this.items;
        }


        public double getTotalPrice()
        {
            double sum = 0;
            for(int i = 0; i < items.Count; i++)
            {
                sum += items[i].product.Price* items[i].Quantity;
            }
            return sum;
        }

    }
}
