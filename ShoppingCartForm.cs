﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Xml;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace BookStore
{
    public partial class ShoppingCartForm : Form , Observer
    {
        Customer customer = Customer.getInstance();

        ShoppingCart shoppingCart;
        Ixml ixml = Ixml.getInstance();
        Repository repository = Repository.getInstance();
        Subject subject = Subject.getInstance();
        public ShoppingCartForm()
        {
            InitializeComponent();
        }

        public ShoppingCartForm(ShoppingCart sc)
        {
            InitializeComponent();
            shoppingCart = sc;
        }

        private void ShoppingCartForm_Load(object sender, EventArgs e)
        {

            dgvShopping.Columns.Add("id", "id");
            dgvShopping.Columns.Add("Name", "Name");
            dgvShopping.Columns.Add("Price", "Price");
            dgvShopping.Columns.Add("Amount", "Amount");
            dgvShopping.EnableHeadersVisualStyles = false;
            dgvShopping.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(48, 51, 56);
            dgvShopping.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(228, 228, 232);
            DataGridViewImageColumn imgcolumn = new DataGridViewImageColumn();
            imgcolumn.Name = "Image";
            imgcolumn.HeaderText = "Image";
            dgvShopping.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            dgvShopping.Columns.Add(imgcolumn);
            DataGridViewRow dgvrow = dgvShopping.RowTemplate;
            dgvrow.Height = 90;
            imgcolumn.ImageLayout = DataGridViewImageCellLayout.Stretch;
            shoppingCart.PrintProducts(dgvShopping);
            dgvShopping.Columns[0].Visible = false;
            subject.Attach(this);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Shopping Cart Back");
            repository.UploadShoppingCartToDB(shoppingCart);
            this.Hide();
            subject.Detach(this);
            
        }

        private void btn_removeitems_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Shopping Cart Remove Items");
            shoppingCart.RemoveProduct(dgvShopping.SelectedCells[0].RowIndex);
            dgvShopping.Rows.Remove(dgvShopping.SelectedCells[0].OwningRow);          
        }

        private void btn_changeamount_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Shopping Cart Change Amount");
            try{
                if (int.Parse(txt_amount.Text) < 0 || repository.getProduct(int.Parse(dgvShopping.SelectedCells[0].OwningRow.Cells[0].Value.ToString())).Stock < int.Parse(txt_amount.Text))
                {
                    MessageBox.Show("You can't buy this amount of item!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    shoppingCart.setQuantity(int.Parse(dgvShopping.SelectedCells[0].OwningRow.Cells[0].Value.ToString()), int.Parse(txt_amount.Text));
                    subject.Notify();
                }
            }
            catch (Exception) { }
        }

        private void btn_buy_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("Shopping Cart Buy");

            if (shoppingCart.GetShoppingCartContents().Count == 0)
            {
                MessageBox.Show("Your shopping cart is empty!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                PaymentForm pf = new PaymentForm(shoppingCart);
                pf.Show();
                this.Hide();
                subject.Detach(this);
            }
        }

        
        public void update()
        {
            if(dgvShopping.Rows.Count>1)
            dgvShopping.SelectedCells[0].OwningRow.Cells[3].Value = shoppingCart.getQuantity(int.Parse(dgvShopping.SelectedCells[0].OwningRow.Cells[0].Value.ToString()));
        }


        private void dgvShopping_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("ShoppingCart Minimize");
            WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ButtonClickLogger btg = new ButtonClickLogger();
            btg.LogButton("ShoppingCart Close Window");
            repository.UploadShoppingCartToDB(shoppingCart);
            this.Close();
            subject.Detach(this);
        }
    }
}
