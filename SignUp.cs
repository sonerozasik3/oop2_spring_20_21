﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookStore
{
    public partial class SignUp : Form
    {
        // Customer customer = Customer.getInstance();
        Repository repository = Repository.getInstance();
        public SignUp()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool flag = true;
            if (txt_address.Text == "" || txt_email.Text == "" || txt_name.Text == "")
            {
                warningText.Text = "Fill all informations!";
            }
            else
            {
                if (username_txt.Text.Length < 3) { warningText.Text = "Your username must be at least 3 characters!"; warningText.ForeColor = Color.Red; flag = false; }
                else if (pass_txt.Text.Length < 6) { warningText.Text = "Your password must be at least 6 characters!"; warningText.ForeColor = Color.Red; flag = false; }
                else if (confirm_txt.Text == "") { warningText.Text = "Please confirm your password!"; warningText.ForeColor = Color.Red; flag = false; }
                else
                {
                    if (checkSpace(username_txt.Text) || checkSpace(pass_txt.Text)) { warningText.Text = "You can't use white space character!"; warningText.ForeColor = Color.Red; }
                    else
                    {
                        if (repository.checkUsernameExist(username_txt.Text)) { warningText.Text = "This Username " + username_txt.Text + " is already taken!"; warningText.ForeColor = Color.Red; flag = false; }
                        else
                        {
                            if (pass_txt.Text != confirm_txt.Text) { warningText.Text = "Passwords does not match!"; warningText.ForeColor = Color.Red; flag = false; }
                            else
                            {

                                warningText.Text = "Succesfully!"; warningText.ForeColor = Color.Green; flag = false;
                                repository.addCustomer(txt_name.Text, txt_email.Text, txt_address.Text, username_txt.Text, pass_txt.Text);
                                MessageBox.Show("Sing Up Succesfully!", "Succesfully!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                Login login = new Login();
                                login.Show();
                                this.Close();
                            }
                        }
                    }
                }
            }
        }
        public bool checkSpace(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == ' ')
                {
                    return true;
                }
            }
            return false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
