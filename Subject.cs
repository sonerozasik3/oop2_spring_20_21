﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore
{
    /// <summary>
    /// Subject class or Observer Pattern.
    /// </summary>
    public class Subject
    {
        private static Subject instance=null;
        private Subject(){ }
        /// <summary>
        /// Singleton constuctor for Subject class.
        /// </summary>
        /// <returns></returns>
        public static Subject getInstance()
        {
            if (instance == null)
            {
                instance = new Subject();
            }
            return instance;
        }

        List<Observer> observers = new List<Observer>();
        /// <summary>
        /// Attach new observer.
        /// </summary>
        /// <param name="o"></param>
        public void Attach(Observer o)
        {
            observers.Add(o);
        }
        /// <summary>
        /// Detach existing observer.
        /// </summary>
        /// <param name="o"></param>
        public void Detach(Observer o)
        {
            observers.Remove(o);
        }
        /// <summary>
        /// Notify observers.
        /// </summary>
        public void Notify()
        {
            for(int i = 0; i < observers.Count; i++)
            {
                observers[i].update();
            }
        }
    }
}
